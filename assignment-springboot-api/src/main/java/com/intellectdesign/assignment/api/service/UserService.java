package com.intellectdesign.assignment.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.intellectdesign.assignment.api.exception.InvalidDataException;
import com.intellectdesign.assignment.api.model.RestError;
import com.intellectdesign.assignment.api.model.RestResponse;
import com.intellectdesign.assignment.api.model.User;
import com.intellectdesign.assignment.api.util.APICommonUtility;
import com.intellectdesign.assignment.api.util.RandomIDGenerator;

@Service
public class UserService {
	
	public static HashMap<String,User> hmUser = new HashMap<String,User>();
	public static List<String> emailIdList = new ArrayList<String>();
	
	@Autowired
	RandomIDGenerator randomIDUtil;
	
	@Autowired
	APICommonUtility apiCommonUtil;
	
	public User createUser(User user) throws InvalidDataException {
		
		if(emailIdList.contains(user.getEmail())) {
			
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"email", 
					"User email is already exist.",
					user.getId());
			
			throw new InvalidDataException(exceptionResponse);
		}
		String userId = randomIDUtil.generateRandomId();
			user.setId(userId);
			user.setActive(true);
		hmUser.put(userId, user);
		emailIdList.add(user.getEmail());
		return user;
	}
	
	public User updateUser(User user,String userId)throws InvalidDataException {
		User oldUser = hmUser.get(userId);
		if(oldUser.getId() == null) {
			
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"Invalid userId.",
					userId);
			
			throw new InvalidDataException(exceptionResponse);
		}
			
		if(!oldUser.isActive()) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"You can not modify data of deactivated user.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
			oldUser.setBirthDate(user.getBirthDate());
			oldUser.setPinCode(user.getPinCode());
		hmUser.put(oldUser.getId(), oldUser);
		return oldUser;
	}
	
	public void disableUser(String userId) throws InvalidDataException {
		User user = hmUser.get(userId);
		if(user == null) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"Invalid userId.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
			
		if(!user.isActive()) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"User is already deactivated.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
		user.setActive(false);
		hmUser.put(user.getId(), user);
	}
	
	
}
