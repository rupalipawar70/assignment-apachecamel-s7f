package com.intellectdesign.assignment.api.util;

public enum EFileExtension {
	
	JSON(".json");
	
	private String constant;

	EFileExtension(String value) {
		this.constant = value;
	}
	
	public String getRestAPIConstants() {
		return constant;
	}

}
