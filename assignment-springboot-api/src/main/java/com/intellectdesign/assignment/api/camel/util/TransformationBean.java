package com.intellectdesign.assignment.api.camel.util;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.intellectdesign.assignment.api.model.RestResponse;
import com.intellectdesign.assignment.api.model.User;
import com.intellectdesign.assignment.api.service.UserService;

public class TransformationBean {
	
	@Autowired
	UserService userService;
	
	public String getUserObj(String jsonStr) {
		String jsonResponse = "";
//		System.out.println("Input file content : " + jsonStr);
		Gson customGson = new GsonBuilder().serializeNulls().create();
		RestResponse restResponse = customGson.fromJson(jsonStr, new TypeToken<RestResponse>(){}.getType());
			
		String userId = restResponse.getUserId();
		if( userId != null) {
			User user = userService.hmUser.get(userId);
			if( user != null) {
				try {
					jsonResponse = customGson.toJson(user,new TypeToken<User>(){}.getType());
//					System.out.println("jsonResponse content : " + jsonResponse);
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
		}
//		System.out.println("output file content : " + jsonResponse);
		return jsonResponse;
	}
}
