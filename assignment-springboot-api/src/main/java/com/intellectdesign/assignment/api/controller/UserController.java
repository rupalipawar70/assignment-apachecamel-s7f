package com.intellectdesign.assignment.api.controller;

import java.io.IOException;
import java.util.List;

import org.apache.camel.CamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.intellectdesign.assignment.api.exception.CamelContextException;
import com.intellectdesign.assignment.api.exception.InvalidDataException;
import com.intellectdesign.assignment.api.model.RestError;
import com.intellectdesign.assignment.api.model.RestResponse;
import com.intellectdesign.assignment.api.model.User;
import com.intellectdesign.assignment.api.service.UserService;
import com.intellectdesign.assignment.api.util.APICommonUtility;
import com.intellectdesign.assignment.api.util.ApacheCamelUtil;
import com.intellectdesign.assignment.api.util.FileUtil;

@RestController
@RequestMapping(value = "/v1/users")
public class UserController {
	
	@Autowired
	UserService userService;
	@Autowired
	APICommonUtility apiCommonUtil;
	@Autowired
	FileUtil fileUtil;
	@Autowired
	ApacheCamelUtil apacheCamelUtil;
	
	static Gson customGson = new GsonBuilder().serializeNulls().create();
	
	/**This method create an user.
	 * @param user
	 * @return ResponseEntity<RestResponse>
	 * @throws InvalidDataException 
	 * @throws IOException 
	 * @throws CamelContextException 
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<RestResponse> createUser(@RequestBody User user) throws InvalidDataException, IOException, CamelContextException{	
		
		RestResponse restResponse = null;
		
		if(user == null){
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"",
					"User entity is null.",
					null);
			throw new InvalidDataException(exceptionResponse);
		}
//		Validate input field
		RestResponse validateResp = apiCommonUtil.validateInputField(restResponse, user);
		if(validateResp != null)
			return new ResponseEntity<RestResponse>(validateResp, HttpStatus.UNPROCESSABLE_ENTITY);
		
		User newUser = userService.createUser(user);
		restResponse = new RestResponse();
			restResponse.setResMsg("User created successfully.");
			restResponse.setUserId(newUser.getId());
			restResponse.setValErrors(null);
			
		//Write a response into input file
			String restResponseStr = customGson.toJson(restResponse,new TypeToken<RestResponse>(){}.getType());
			fileUtil.saveDataIntoFile(restResponse.getUserId(),restResponseStr);
		//Invoke camel endpoint
			apacheCamelUtil.addRouteIntoCamelContext();
		
		return new ResponseEntity<RestResponse>(restResponse, HttpStatus.CREATED); 
	}
	
		
	/**This method update an user.
	 * @param userId
	 * @param user
	 * @return ResponseEntity<RestResponse>
	 * @throws InvalidDataException 
	 * @throws IOException 
	 * @throws CamelContextException 
	 */
	@RequestMapping(value = "/update/{userId}", method = RequestMethod.PATCH, 
			consumes = "application/json")
	public ResponseEntity<RestResponse> updateUser(@PathVariable("userId")String userId,
			@RequestBody User user) throws InvalidDataException, IOException, CamelContextException {
		
		RestResponse restResponse = null;
		
		if(userId == null){
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"Invalid userId.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
		if(user == null){
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"",
					"User entity is null.",
					null);
			throw new InvalidDataException(exceptionResponse);
		}
		
		//validate input field
		List<RestError> valErrorList = apiCommonUtil.validateFields(null, null, null, user.getPinCode(), user.getBirthDate());
		if(valErrorList.size() > 0 ) {
			//Return mandatory field validation error response
			restResponse = new RestResponse();
			restResponse.setResMsg("Input field validation error.");
			restResponse.setUserId(userId);
			restResponse.setValErrors(valErrorList);
			return new ResponseEntity<RestResponse>(restResponse, HttpStatus.UNPROCESSABLE_ENTITY);
		}
		
		User updatedUser = userService.updateUser(user, userId);
		
		restResponse = new RestResponse();
		restResponse.setResMsg("User updated successfully.");
		restResponse.setUserId(updatedUser.getId());
		restResponse.setValErrors(null);
		//Write a response into input file
			String restResponseStr = customGson.toJson(restResponse);
			fileUtil.saveDataIntoFile(restResponse.getUserId(),restResponseStr);
		//Invoke camel endpoint
			apacheCamelUtil.addRouteIntoCamelContext();
		
		return new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK); 
	}
	
	/**This method disable an user
	 * @param userId
	 * @return ResponseEntity<RestResponse>
	 * @throws InvalidDataException 
	 * @throws CamelContextException 
	 * @throws IOException 
	 */
	@RequestMapping(value = "/disable/{userId}", method = RequestMethod.DELETE, 
			consumes = "application/json")
	public ResponseEntity<RestResponse> disableUser(@PathVariable("userId")
		String userId) throws InvalidDataException, CamelContextException, IOException {
		
		RestResponse restResponse = null;
		if(userId == null) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"Invalid userId.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
		userService.disableUser(userId);
		
		restResponse = new RestResponse();
		restResponse.setResMsg("User is deactivated.");
		restResponse.setUserId(userId);
		restResponse.setValErrors(null);
		//Write a response into input file
		String restResponseStr = customGson.toJson(restResponse);
		fileUtil.saveDataIntoFile(restResponse.getUserId(),restResponseStr);
		//Invoke camel endpoint
		apacheCamelUtil.addRouteIntoCamelContext();
		return new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
	}

}
