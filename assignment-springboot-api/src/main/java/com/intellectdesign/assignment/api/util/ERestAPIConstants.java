package com.intellectdesign.assignment.api.util;

public enum ERestAPIConstants {
	
	USER_CONTROLLER_V1("/v1/users");
	
	private String constant;

	ERestAPIConstants(String value) {
		this.constant = value;
	}
	
	public String getRestAPIConstants() {
		return constant;
	}
}
