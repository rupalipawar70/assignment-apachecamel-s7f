package com.intellectdesign.assignment.api.camel.util;

import org.apache.camel.builder.RouteBuilder;


public class IntegrationRoute extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		
		TransformationBean transformationBean = new TransformationBean();
		LoggingProcessor loggingProcessor = new LoggingProcessor();
		// Consumer Endpoint
		from("file:camel-activity/input?noop=true")
			.process(loggingProcessor)
			.bean(transformationBean,"getUserObj")
		// Producer Endpoint
			.to("file:camel-activity/output");
	}

}
