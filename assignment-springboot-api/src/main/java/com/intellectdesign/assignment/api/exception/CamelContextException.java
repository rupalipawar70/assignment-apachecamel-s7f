package com.intellectdesign.assignment.api.exception;

public class CamelContextException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2848079260240740399L;

	public CamelContextException(String message, Throwable cause) {
		super(message, cause);
	}

	public CamelContextException(String message) {
		super(message);
	}
}
