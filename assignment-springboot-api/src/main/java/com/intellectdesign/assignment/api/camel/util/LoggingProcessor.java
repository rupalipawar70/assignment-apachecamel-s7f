package com.intellectdesign.assignment.api.camel.util;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.intellectdesign.assignment.api.model.RestResponse;
import com.intellectdesign.assignment.api.model.User;
import com.intellectdesign.assignment.api.service.UserService;

public class LoggingProcessor implements Processor {
	
	@Override
	public void process(Exchange exchange) throws Exception {
//		System.out.println("Received Request: " + exchange.getIn().getBody(String.class));
	}

}
