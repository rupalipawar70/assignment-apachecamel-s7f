package com.intellectdesign.assignment.api.util;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.intellectdesign.assignment.api.camel.util.IntegrationRoute;
import com.intellectdesign.assignment.api.exception.CamelContextException;
import com.intellectdesign.assignment.api.exception.InvalidDataException;

@Service
public class ApacheCamelUtil {

//	static CamelContext camelContext = null;
	
	@Autowired
	APICommonUtility apiCommonUtil;
	
	public CamelContext getCamelContext() {
		
		CamelContext camelContext = new DefaultCamelContext(); 
		return camelContext;
	}
	
	public void addRouteIntoCamelContext() throws CamelContextException {
		try {
//			if(camelContext == null)
			CamelContext camelContext = getCamelContext();
			camelContext.addRoutes(new IntegrationRoute());
			camelContext.start();
			Thread.sleep(30000);
			camelContext.stop();
		}catch (Exception e) {
			e.printStackTrace();
//			String exceptionResponse = apiCommonUtil.createRestResponseString(
//					HttpStatus.SERVICE_UNAVAILABLE.toString(), 
//					"",
//					e.getMessage(),
//					null);
//			throw new CamelContextException(exceptionResponse);
		}
	}
}
