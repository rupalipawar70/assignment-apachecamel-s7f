package com.intellectdesign.assignment.api.exception;

public class InvalidDataException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7804504727856622390L;
	
	public InvalidDataException(String message, Throwable cause) {
		super(message, cause);	
	}

	public InvalidDataException(String message) {
		super(message);
	}

	

	
}
