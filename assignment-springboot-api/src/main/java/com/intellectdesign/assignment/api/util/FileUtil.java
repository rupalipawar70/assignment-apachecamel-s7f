package com.intellectdesign.assignment.api.util;

import java.io.FileWriter;
import java.io.IOException;

import org.springframework.stereotype.Service;

@Service
public class FileUtil {
	
	public void saveDataIntoFile(String fileName, String data) throws IOException {
		FileWriter file = null;
		try  {
			String filePath = "./camel-activity/input/" + fileName + ".json";
//			File f = new File(filePath);
//			if(!f.exists()) { 
//			   f.createNewFile(); 
//			}
			file = new FileWriter(filePath);
			file.write(data);
			file.flush();
            file.close();
        } catch (IOException e) {
            throw e;
        }
	}

}
